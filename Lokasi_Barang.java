package com.sifina;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;


public class Lokasi_Barang extends Activity {
	 /** Called when the activity is first created. */
    EditText ed1;
    EditText ed2;
	Button button;
	RadioGroup JenisKelamin;
	RadioButton radioSexButton;
	TextView h;
	Spinner Spinner01,Spinner02,Spinner03;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sub_lokasi_barang);
        
        EditText ed1 = (EditText) findViewById(R.id.ed1);
        EditText ed2 = (EditText) findViewById(R.id.ed2);
        final TextView h=(TextView) findViewById(R.id.h);
        
        Spinner01 = (Spinner)findViewById  (R.id.Spinner01);
        Spinner02 = (Spinner)findViewById  (R.id.Spinner02);
        Spinner03 = (Spinner)findViewById  (R.id.Spinner03);
    	button = (Button) findViewById(R.id.button1);
     
    	String[] array = {"Minuman","Makanan","Hardware","Elektronik"};
    	ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, array);        
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner01.setAdapter(adapter);
        
        String[] array1 = {"1","2","3","4"};
    	ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, array1);        
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner01.setAdapter(adapter1);
        
        String[] array2 = {"1","2","3","4"};
    	ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, array2);        
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner01.setAdapter(adapter2);
//    	btnDisplay.setOnClickListener(new OnClickListener() {
//     
//    		@Override
//    		public void onClick(View v) {
//     
//    		        // get selected radio button from radioGroup
//    			int selectedId = JenisKelamin.getCheckedRadioButtonId();
//     
//    			// find the radiobutton by returned id
//    		        JenisKelamin = (RadioButton) findViewById(selectedId);
//     
//    			Toast.makeText(MyAndroidAppActivity.this,
//    				radioSexButton.getText(), Toast.LENGTH_SHORT).show();
//     
//    		}
//     
//    	});
//     
//      }
        
        
        Button button = (Button) findViewById(R.id.button1);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				AlertDialog.Builder alertKeluar = new AlertDialog.Builder(Lokasi_Barang.this);
				alertKeluar.setMessage(
						"Data akan disimpan\n"
						)
						.setCancelable(false)
						.setPositiveButton("Simpan", new AlertDialog.OnClickListener(){
							public void onClick(DialogInterface arg0, int arg1) {
								Intent i = new Intent(Lokasi_Barang.this, MenuUtama.class);
								startActivity(i);
							}

						})
						.setNegativeButton("Batal",new AlertDialog.OnClickListener(){

							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();

							}

						});
				AlertDialog a = alertKeluar.create();
				a.setTitle("HELP");
				a.setIcon(R.drawable.ic_launcher);
				a.show();
//				h.setText("Data Telah Tersimpan");
//				
//				finish();  // same as "back" .. either way we get onPause() to save
			}
		});
    }
}
